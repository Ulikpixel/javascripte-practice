//---------text---------//
const field   = document.querySelector('.field'),
 	  text 	  = document.querySelector('.result'),
 	  number  = document.querySelector('.num'),
	  warning = document.querySelector('.warning')
	  warningText = "вы ввели 6 максимально количество символов!";

field.oninput = () => {
  const val = field.value;
  text.innerText = val;
  number.innerText = val.length;
  val.length > 5 ? warning.innerText = warningText : warning.innerText = "";
}

//------- modal window --------//
const btnModal 	 = document.querySelector('.modal__window'),
	  closeModal = document.querySelector('#close'),
	  bodyModal  = document.querySelector('.modal__body'),
	  myModal	 = document.querySelector('.modal__wrapper');

btnModal.onclick   = () => myModal.style.display = "block";
bodyModal.onclick  = () => myModal.style.display = "none";;
closeModal.onclick = () => myModal.style.display = "none";

//--------work with check box-------//
const checkboxs = document.querySelectorAll('.checkboxs'),
	  checkText = document.querySelector('.check__text');

	checkboxs.forEach(item => {
		item.addEventListener('change', () => {
			if(checkboxs[0].checked){
				checkText.innerText = 'В ягодах содержится много минералов, которые нужны для здоровья костной ткани.'
			}
			else if(checkboxs[1].checked){
				checkText.innerText = "Фруктах в основном содержится витамин A";
			}
			else if(checkboxs[2].checked){
				checkText.innerText = "В овощах содержится почти все виды витаминов!";
			}
			else {
				checkText.innerText = "ничего не выбрано!";
			}
		})
	})

//--------prompt---------//
const btnAge  = document.querySelector('.age__btn'),
	  textAge = document.querySelector('.age');

let age = () => age = prompt("сколько вам лет?");

btnAge.addEventListener('click', () => {
	age()
	if(age > 18 && age < 60) {
		textAge.innerText = " вы взрослый человек";
	}
	else if(age < 18 && age > 0) {
		textAge.innerText = " ты еще щенок";
	}	
	else if(age > 60) {
		textAge.innerText = ' ты старье';
	} 
	else if(age == 0 || age < 0) {
		textAge.innerText = ' вы ничего не ввели!';
	}	
	else {
		textAge.innerText = ' напиши цифры идиот!';
	}
});

//--------tabs--------//
let tab = () => {
	const tabNav     = document.querySelectorAll('.tab'),
		  tabContent = document.querySelectorAll('.tab__content');
		  
	let tabName;

	tabNav.forEach(item => item.addEventListener('click', selectTabNav));

	function selectTabNav() {
		tabNav.forEach(item => item.classList.remove('is-active'));
		this.classList.add('is-active');
		tabName = this.getAttribute('data-tab-name');
		selectTabContent(tabName);
	}

	function selectTabContent(tabName) {
		tabContent.forEach(item => {
			item.classList.contains(tabName) ? item.classList.add('is-active'):
			item.classList.remove('is-active');
		})
	}
};
tab()

//--------slider---------//

const imgTotal  = document.querySelectorAll('.slider__field img'),
	  btnRight  = document.querySelector('.slider__right'),
	  btnLeft   = document.querySelector('.slider__left');

let activeNum = 0;

btnRight.addEventListener('click', moveRight);
btnLeft.addEventListener('click', moveLeft);


function moveRight() {
	if (activeNum < imgTotal.length) {
		activeNum++;
	}
	imgTotal.forEach(item => {
		item.className = "";
		imgTotal[activeNum].className = "opacity"
	});
}

function moveLeft() {
	if (activeNum > 0) {
		activeNum--;
}
	imgTotal.forEach(item => {
		item.className = "";
		imgTotal[activeNum].className = "opacity"
	});
}

//--------calculator--------//
const calcField = document.querySelector('.calc__field'),
	  calcNum   = document.querySelectorAll('.item__num');
		
calcNum.forEach(item => {
  item.addEventListener('click', (e) => {
  	let btnVal = item.innerHTML;
   	if(btnVal == "delete"){
   		calcField.value = "";
   	}else if(btnVal == "="){
   		calcField.value = eval(calcField.value)
   	}else{
   		calcField.value += btnVal;
   	}
   	e.preventDefault()
  });
});

//-------css generator-------//
let genBlock = document.querySelector('.gen__block'),
 	genItem  = document.querySelectorAll('#gen__item'),
	rtl      = document.querySelector('.rtl'),
	ttl      = document.querySelector('.ttl'),
	rtr      = document.querySelector('.rtr'),
	ttr      = document.querySelector('.ttr'),
	rbr      = document.querySelector('.rbr'),
	tbr      = document.querySelector('.tbr'),
	rbl      = document.querySelector('.rbl'),
	tbl      = document.querySelector('.tbl');

genItem.forEach(item => item.oninput = () => gen());

function gen() {
	ttl.value = rtl.value;
	ttr.value = rtr.value;
	tbr.value = rbr.value;
	tbl.value = rbl.value;
	genBlock.style.borderRadius = rtl.value + "px " + rtr.value + "px " + rbr.value + "px " + rbl.value + "px ";
}

//------- collapse --------//
const collBtn  	  = document.querySelectorAll('.collapse__btn'),
	  collContent = document.querySelectorAll('.collapse__content');

collBtn.forEach(item => {
	item.addEventListener('click', () => {
		item.classList.toggle("collapse__active");
		let content = item.nextElementSibling;
		if(content.style.maxHeight){
			content.style.maxHeight = null;
		}else{
			content.style.maxHeight = content.scrollHeight + 'px';
		}
	});
});


//-------To Do List------//


let addToDo   = document.querySelector('.ToDo__add'),
	textToDo  = document.querySelector('.ToDo__text'),
	listToDo  = document.querySelector('.ToDo__list'),
	clearToDo = document.querySelector('.ToDo__clear'),
	hideToDo  = document.querySelector('.ToDo__hide'),
	NumToDo   = document.querySelector('.list__num'),
	arr = [];

addToDo.addEventListener('click', () => {
	arr.push(textToDo.value);
	textToDo.value = "";
	listToDo.innerHTML = "";
	addList()
});

function addList(){
	for(let i = 0; i < arr.length; i++){
		let p = document.createElement('p');
		var check = document.createElement('input');
		check.type = "checkbox";
		p.innerHTML = arr[i];
		p.prepend(check);
		listToDo.appendChild(p);
		NumToDo.innerText = arr.length;
		check.addEventListener('change', () => {
			p.classList.toggle("check__list");
		});
	}
}

clearToDo.onclick = () => {
	listToDo.innerHTML = ""; arr = [];
	NumToDo.innerText = arr.length;
}

hideToDo.onclick = () => listToDo.classList.toggle('hide');

//----- sidebar -----//
const btnMenu = document.querySelector('.toggle'),
	  sidebar = document.querySelector('.sidebar');
	
btnMenu.onclick = () => sidebar.classList.toggle('open-menu');

//----- Hamburger menu -----//
const burgerMenu = document.querySelector('.hamburger');

burgerMenu.onclick = () => burgerMenu.classList.toggle('burger-open');

//----- Basket -----//
let productsData = [
	{ title: 'potato', price: 3 },
	{ title: 'cucumber', price: 7 },
	{ title: 'tomato', price: 8 },
	{ title: 'products', price: 0 },
]

let btnBasket    = document.querySelectorAll('.price__add'),
	prices       = document.querySelectorAll('.price span'),
	pricesResult = document.querySelectorAll('.price__result span');

prices[0].innerHTML = productsData[0].price
prices[1].innerHTML = productsData[1].price
prices[2].innerHTML = productsData[2].price

btnBasket.forEach(item => {
	item.addEventListener('click', () => {
		if(item.id == 'potato'){
			pricesResult[0].innerHTML = productsData[0].price
			productsData[0].price += 3
			productsData[3].price += 3
			pricesResult[3].innerHTML = productsData[3].price
		}
		else if(item.id == 'cucumber'){
			pricesResult[1].innerHTML = productsData[1].price
			productsData[1].price += 7
			productsData[3].price += 7
			pricesResult[3].innerHTML = productsData[3].price
		}
		else if(item.id == 'tomato'){
			pricesResult[2].innerHTML = productsData[2].price
			productsData[2].price += 8
			productsData[3].price += 8
			pricesResult[3].innerHTML = productsData[3].price
		}
	})
})

//----- owl carousel -----//
const carouselTrack = document.querySelector('.carousel__track'),
	  carouselDots  = document.querySelectorAll('.carousel__dots span'),
	  carouselPrev  = document.querySelector('.carousel__prev'),
	  carouselNext  = document.querySelector('.carousel__next');

let dotsNum  = 0;
let position = 0;

carouselNext.addEventListener('click', () => {
	dotsNext();
	position == 200 * 2 ? position = 0 : position += 200;
	carouselTrack.style.transform = `translateX(-${position}px)`;
});

carouselPrev.addEventListener('click', () => {
	dotsPrev();
	position == 0 ? position = 200* 2 : position -= 200;
	carouselTrack.style.transform = `translateX(-${position}px)`;
});

// carousel dots
function dotsPrev() {
	carouselDots[dotsNum].classList.remove('active-dot');
	dotsNum - 1 == -1 ? dotsNum = carouselDots.length - 1 : dotsNum--
	carouselDots[dotsNum].classList.add('active-dot');
};

function dotsNext() {
	carouselDots[dotsNum].classList.remove('active-dot');
	dotsNum == carouselDots.length - 1 ? dotsNum = 0 : dotsNum++
	carouselDots[dotsNum].classList.add('active-dot');
};

carouselDots.forEach(item => {
	item.addEventListener('click', () => {
		if(item.id == 'dot__one'){
			dotsNum  = 0;
			position = 0;
			nextDots()
		}
		else if(item.id == 'dot__two'){
			dotsNum  = 1;
			position = 200;
			nextDots()			
		}
		else if(item.id == 'dot__three'){
			dotsNum  = 2;
			position = 200 * 2;
			nextDots()
		}
	});
});

function nextDots(){
	carouselDots.forEach(item => {
		item.classList.remove('active-dot');
	});
	carouselDots[dotsNum].classList.add('active-dot');
	carouselTrack.style.transform = `translateX(-${position}px)`;
};